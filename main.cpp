#include <iostream>

int main() {
    // 如果对象作为函数的参数，在调用函数的时候，会引发对象的拷贝哦构造函数，
    // 所以需要使用对象的引用，完成函数的调用
    std::cout << "Hello, World!" << std::endl;
    return 0;
}